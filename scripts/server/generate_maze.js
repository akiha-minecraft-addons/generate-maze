// ユーザー設定
const settings = {
  // 壁の種類
  block: {
    wall: 'planks 0', // 壁
    entrance: 'diamond_block', // 入口の目印
    exit: 'gold_block', // 出口の目印
  },
  // 迷路内の大きさ（さらに周囲1ブロック分の壁が加わる）
  // 大きすぎると動かない可能性がある
  size: {
    x: 21, // 奇数
    y: 3, // 任意
    z: 21, // 奇数
  },
};

/* global server */
const system = server.registerSystem(0, 0);

// 実行するコマンドを入れる配列
const commands = [];

// 空気ブロック
const AIR = 'minecraft:air';

/**
 * Slash Command
 */

function setBlock(pos, block) {
  commands.push(`/setblock ${pos.x} ${pos.y} ${pos.z} ${block}`);
}

// 一度に2^15以上のブロックを置かないように注意
function fill(from, to, block) {
  const pos1 = `${from.x} ${from.y} ${from.z}`;
  const pos2 = `${to.x} ${to.y} ${to.z}`;
  commands.push(`/fill ${pos1} ${pos2} ${block}`);
}

/**
 * Utilities
 */

// オブジェクトをディープコピー
// https://qiita.com/k_2929/items/0ae5085d61fa5598c76e
function copyObj(obj) {
  return JSON.parse(JSON.stringify(obj));
}

// 配列を無作為な順序で並べ替える
// https://www.delftstack.com/ja/howto/javascript/shuffle-array-javascript/
function fisherYatesShuffle(arr) {
  for (let i = arr.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
  return arr;
}

/**
 * 迷路生成クラス
 */
class Maze {
  // 最初に実行されるメソッド
  constructor(basePos) {
    // 開始座標
    this.basePos = basePos;

    // 定数は大文字
    // 向き座標。それぞれの(x, z)
    this.DIRECTIONS = [
      { x: 1, z: 0 }, // 東
      { x: 0, z: 1 }, // 南
      { x: -1, z: 0 }, // 西
      { x: 0, z: -1 }, // 北
    ];
    // (0(東), 1(南), 2(西), 3(北))の逆向き
    this.REVERSE = [2, 3, 0, 1];

    // 未探索箇所を保存する配列
    this.stack = [];
    // 探索状況を保存する配列
    this.grid = {};
    // 探索済み
    this.visitedList = [];

    // 迷路基準の大きさに調整
    this.gridSize = {
      x: Math.ceil(settings.size.x / 2),
      z: Math.ceil(settings.size.z / 2),
    };
  }

  // 2次元配列で座標を作り、その座標にオブジェクトで情報を持たせる
  initializeGrid() {
    // 繰り返しで2次元配列を作る
    const c = {};
    for (let x = 0; x < this.gridSize.x; x += 1) {
      c[x] = {};
      for (let z = 0; z < this.gridSize.z; z += 1) {
        c[x][z] = {
          // 各方向は壁であるか（最初に全て壁と考えて、通る場所を偽にして穴を開けていく）
          isWall: [true, true, true, true],
          // 探索済みであるか（初期値なので偽）
          isVisited: false,
        };
      }
    }
    this.grid = c;
  }

  // 範囲内判定: その位置(pos)は範囲の内側であるか
  isInside(pos) {
    const isInX = (pos.x >= 0) && (pos.x < this.gridSize.x);
    const isInZ = (pos.z >= 0) && (pos.z < this.gridSize.z);
    return isInX && isInZ;
  }

  // 探索済み判定: 内側でない（外側なら探索済み扱いにする）または探索済みか
  isVisited(pos) {
    return (!this.isInside(pos) || this.grid[pos.x][pos.z].isVisited);
  }

  // その地点(pos)からある方向(d)の座標を取得する
  move(pos, d) {
    const x = pos.x + this.DIRECTIONS[d].x;
    const z = pos.z + this.DIRECTIONS[d].z;
    return { x, z };
  }

  // 各方向の探索状況を配列にして返す
  getVisitedList(pos) {
    this.visitedList = [];
    // d＝0,1,2,3でループ
    for (let d = 0; d < this.DIRECTIONS.length; d += 1) {
      // 東西南北の箇所の状態を調べる
      this.visitedList.push(this.isVisited(this.move(pos, d)));
    }
  }

  // まだ探索していない方角を一つ選ぶ
  getNextDirection() {
    // 方向番号となる配列を作り無作為に並べ替え
    const arr = fisherYatesShuffle([0, 1, 2, 3]);
    // 変更された順番で方向を調べる
    for (let i = 0; i < arr.length; i += 1) {
      // その方向が未探索であれば（探索済みでなければ）
      if (!this.visitedList[arr[i]]) {
        // その方向番号を返してループ終了
        return arr[i];
      }
    }
    // ここにはたどり着かないはず
    system.displayChat('error');
    return false;
  }

  // 壁となる部分にブロックを置く
  setWall(p, x, z) {
    const from = {
      x: p.x + x,
      y: p.y,
      z: p.z + z,
    };
    const to = copyObj(from)
    to.y += settings.size.y - 1;
    fill(from, to, settings.block.wall);
  }

  // 柱となる部分（斜め方向）にブロックをおく
  setPillar(p) {
    this.setWall(p, +1, +1);
    this.setWall(p, +1, -1);
    this.setWall(p, -1, +1);
    this.setWall(p, -1, -1);
  }

  // 入口を作る
  makeEntrance() {
    const pos = copyObj(this.basePos);
    pos.x -= 1;
    pos.y -= 1;
    // 地面に目印ブロックを置く
    setBlock(pos, settings.block.entrance);
    // 内に入るために壁を壊す
    pos.y += 1;
    setBlock(pos, AIR);
    pos.y += 1;
    setBlock(pos, AIR);
  }

  // 出口を作る
  makeExit() {
    const pos = {
      x: this.basePos.x + settings.size.x,
      y: this.basePos.y - 1,
      z: this.basePos.z + settings.size.z - 1,
    };
    // 地面に目印ブロックを置く
    setBlock(pos, settings.block.exit);
    // 外に出るために壁を壊す
    pos.y += 1;
    setBlock(pos, AIR);
    pos.y += 1;
    setBlock(pos, AIR);
  }

  // 迷路内のブロックを削除
  clearInside() {
    const to = {
      x: this.basePos.x + settings.size.x,
      y: this.basePos.y + settings.size.y - 1,
      z: this.basePos.z + settings.size.z,
    };
    system.displayChat(settings.size.x * settings.size.z * settings.size.y);
    // ここは一度に制限を超えるブロックを置いてしまう可能性があるので、それを超えるようなら実行しない
    if (settings.size.x * settings.size.z * settings.size.y <= (2 ** 15)) {
      fill(this.basePos, to, AIR);
    }
  }

  // ブロックを置く場所を計算する
  generate() {
    this.initializeGrid();
    // 探索開始位置(position)
    // 範囲内ならどこでもいい（多分中央付近にする方が複雑な迷路になる）
    let pos = {
      x: Math.floor(this.gridSize.x / 2),
      z: Math.floor(this.gridSize.z / 2),
    };

    // 条件を満たすまで繰り返し
    let loop = true;
    while (loop) {
      // 今の場所を探索済み判定にする
      this.grid[pos.x][pos.z].isVisited = true;
      // 現在位置からの4方向の探索情報を取得する
      this.getVisitedList(pos);

      // 全て方向を探索していれば、残りの未探索箇所を探す
      if (this.visitedList.every((val) => val === true)) {
        // 未探索リスト(stack)に中身があるか
        if (this.stack.length) {
          // 未探索の箇所を次の位置情報として取り出す
          pos = this.stack.pop();
          // ループ最初に戻る
        } else {
          // 未探索箇所がなければ探索終了
          loop = false;
        }
      } else {
        // 次に探索する方向を取得
        const d = this.getNextDirection();
        // 通る箇所(その位置のその方向(d))には壁を置かない(false)
        this.grid[pos.x][pos.z].isWall[d] = false;
        // その向きに移動
        pos = this.move(pos, d);
        // 移動後の後ろ側にも壁を置かない（上と同じ位置なので）
        this.grid[pos.x][pos.z].isWall[this.REVERSE[d]] = false;
        // 移動先を保存（まだ探索先が残っている可能性があるので）
        this.stack.push(pos);
      }
    }

    // 迷路内のブロックを除去
    this.clearInside();
    // 指定した迷路の大きさでループ
    for (let x = 0; x < this.gridSize.x; x += 1) {
      for (let z = 0; z < this.gridSize.z; z += 1) {
        const p = {
          x: this.basePos.x + (x * 2),
          y: this.basePos.y,
          z: this.basePos.z + (z * 2),
        };
        // 壁部分にブロックを置くループ
        for (let d = 0; d < this.DIRECTIONS.length; d += 1) {
          // 壁を置くべき箇所かどうか
          if (this.grid[x][z].isWall[d]) {
            // 壁判定ならブロックを置く
            this.setWall(p, this.DIRECTIONS[d].x, this.DIRECTIONS[d].z);
          }
        }
        // 柱部分にブロックを置く
        this.setPillar(p);
      }
    }
    // 壁に出入り口を作る
    this.makeEntrance();
    this.makeExit();
  }
}

system.initialize = function initialize() {
  this.listenForEvent(
    'minecraft:player_placed_block',
    (eventData) => this.onPlayerPlacedBlock(eventData),
  );
};

system.update = function update() {
  // 1tickでのコマンド実行制限
  for (let i = 0; i < 40; i += 1) {
    if (commands.length === 0) break;
    this.executeCommand(commands.shift());
  }
};

system.onPlayerPlacedBlock = function onPlayerPlacedBlock(d) {
  // 入口の位置
  const basePos = d.data.block_position;
  // ブロックを置くことが起動条件だが、そのブロックを迷路とは無関係なので消す
  setBlock(basePos, AIR);
  // 置いた場所が入口で、迷路内の基準はその1ブロック先
  basePos.x += 1;
  // 迷路を生成する
  const maze = new Maze(basePos);
  maze.generate();
};

system.displayChat = function displayChat(message) {
  const eventData = this.createEventData('minecraft:display_chat_event');
  eventData.data.message = message;
  this.broadcastEvent('minecraft:display_chat_event', eventData);
};

system.executeCommand = function executeCommand(command) {
  const eventData = this.createEventData('minecraft:execute_command');
  eventData.data.command = command;
  this.broadcastEvent('minecraft:execute_command', eventData);
};
